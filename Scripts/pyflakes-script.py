#!C:/Program Files/Anaconda3/envs/scor_pc\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'pyflakes==1.6.0','console_scripts','pyflakes'
__requires__ = 'pyflakes==1.6.0'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('pyflakes==1.6.0', 'console_scripts', 'pyflakes')()
    )
