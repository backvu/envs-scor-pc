#!C:/Program Files/Anaconda3/envs/scor_pc\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'jupyterlab==0.27.0','console_scripts','jupyter-labhub'
__requires__ = 'jupyterlab==0.27.0'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('jupyterlab==0.27.0', 'console_scripts', 'jupyter-labhub')()
    )
